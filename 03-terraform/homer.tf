resource "kubernetes_namespace_v1" "homer" {
  metadata {
    name = "homer"
  }
}

# https://fontawesome.com/v5/search

module "homer" {
  source = "./modules/homer"

  namespace = kubernetes_namespace_v1.homer.metadata[0].name

  assets_dir = "${path.module}/homer"
  config = {
    title    = "Homelab"
    subtitle = false
    # logo     = ""
    # icon     = ""

    header = false
    footer = false

    services = [
      # {
      #   name = "Applications"
      #   icon = "fas fa-code-branch"
      #   items = [
      #     { name = "qBittorrent", icon = "fas fa-download", url = "/qbittorrent/" },
      #   ]
      # },
      {
        name = "3D Printing"
        icon = "fas fa-tools"
        items = [
          { name = "Ender 3 v2 [01]", logo = "assets/logos/fluidd.svg", url = "http://ender01.lan/" },
        ]
      },
    ]
  }
}

resource "kubernetes_ingress_v1" "homer" {
  metadata {
    name      = "homer"
    namespace = kubernetes_namespace_v1.homer.metadata[0].name
  }

  spec {
    default_backend {
      service {
        name = module.homer.service.metadata[0].name

        port {
          name = "http"
        }
      }
    }
  }
}
