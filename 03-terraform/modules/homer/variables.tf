variable "namespace" {
  type    = string
  default = "default"
}

variable "assets_dir" {
  type    = string
  default = null
}

variable "config" {
  type        = any
  description = "https://github.com/bastienwirtz/homer/blob/main/docs/configuration.md"
}
