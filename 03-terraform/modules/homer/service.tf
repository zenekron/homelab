resource "kubernetes_service_v1" "homer" {
  metadata {
    name      = "homer"
    namespace = var.namespace
  }

  spec {
    selector = kubernetes_deployment_v1.homer.spec[0].selector[0].match_labels

    port {
      port         = 80
      name         = "http"
      protocol     = "TCP"
      target_port  = "http"
      app_protocol = "http"
    }
  }
}
