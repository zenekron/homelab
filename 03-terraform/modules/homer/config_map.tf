locals {
  assets = var.assets_dir == null ? [] : [
    for f in fileset(var.assets_dir, "**") : {
      path = f
      key  = replace(f, "/\\//", ".")
      data = file("${var.assets_dir}/${f}")
    }
  ]
}

resource "kubernetes_config_map_v1" "assets" {
  metadata {
    name      = "assets"
    namespace = var.namespace
  }

  data = {
    "config.yml" = jsonencode(var.config)
  }

  binary_data = {
    for f in local.assets : f.key => base64encode(f.data)
  }
}
