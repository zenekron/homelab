resource "kubernetes_deployment_v1" "homer" {
  metadata {
    name      = "homer"
    namespace = var.namespace
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "app.kubernetes.io/name" = "homer"
      }
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name" = "homer"
        }
      }

      spec {
        container {
          name  = "homer"
          image = "b4bz/homer:latest"

          port {
            name           = "http"
            container_port = 8080
            protocol       = "TCP"
          }

          volume_mount {
            name       = "assets"
            mount_path = "/www/assets"
            read_only  = true
          }
        }

        volume {
          name = "assets"

          config_map {
            name = kubernetes_config_map_v1.assets.metadata[0].name

            items {
              key  = "config.yml"
              path = "config.yml"
            }

            dynamic "items" {
              for_each = local.assets
              iterator = it

              content {
                path = it.value.path
                key  = it.value.key
              }
            }
          }
        }
      }
    }
  }
}
