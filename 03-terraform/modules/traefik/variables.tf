variable "namespace" {
  type    = string
  default = "default"
}

variable "values" {
  type        = any
  default     = {}
  description = "https://github.com/traefik/traefik-helm-chart/blob/master/traefik/values.yaml"
}
