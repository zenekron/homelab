resource "helm_release" "traefik" {
  name      = "traefik"
  namespace = var.namespace

  chart      = "traefik"
  repository = "https://helm.traefik.io/traefik"
  version    = "10.24.0"

  values = [jsonencode(var.values)]
}

# rbac:
#   enabled: true
# ports:
#   websecure:
#     tls:
#       enabled: true
# podAnnotations:
#   prometheus.io/port: "8082"
#   prometheus.io/scrape: "true"
# providers:
#   kubernetesIngress:
#     publishedService:
#       enabled: true
# priorityClassName: "system-cluster-critical"
