output "middlewares" {
  value = {
    for middleware in [kubectl_manifest.strip_prefix_1_middleware]
    : replace(middleware.name, "/-/", "_") => {
      id = "${middleware.namespace}-${middleware.name}@kubernetescrd"
    }
  }
}
