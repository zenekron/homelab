resource "kubectl_manifest" "strip_prefix_1_middleware" {
  depends_on = [helm_release.traefik]

  yaml_body = jsonencode({
    apiVersion = "traefik.containo.us/v1alpha1"
    kind       = "Middleware"

    metadata = {
      name      = "strip-prefix-1"
      namespace = var.namespace
    }

    spec = {
      stripPrefixRegex = {
        regex = ["/[^/]+"]
      }
    }
  })
}
