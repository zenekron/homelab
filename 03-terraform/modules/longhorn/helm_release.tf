resource "helm_release" "longhorn" {
  name      = "longhorn"
  namespace = var.namespace

  chart      = "longhorn"
  repository = "https://charts.longhorn.io"
  version    = "1.3.0"

  values = [jsonencode(var.values)]
}
