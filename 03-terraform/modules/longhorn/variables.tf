variable "namespace" {
  type    = string
  default = "default"
}

variable "values" {
  type        = any
  default     = {}
  description = "https://github.com/longhorn/charts/blob/longhorn-1.3.0/charts/longhorn/values.yaml"
}
