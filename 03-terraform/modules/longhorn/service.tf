data "kubernetes_service_v1" "longhorn_frontend" {
  depends_on = [helm_release.longhorn]

  metadata {
    name      = "longhorn-frontend"
    namespace = var.namespace
  }
}
