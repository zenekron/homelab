resource "kubernetes_namespace_v1" "traefik_system" {
  metadata {
    name = "traefik-system"
  }
}

module "traefik" {
  source = "./modules/traefik"

  namespace = kubernetes_namespace_v1.traefik_system.metadata[0].name
}
