terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.6.0"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.12.1"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.3.2"
    }
  }

  backend "kubernetes" {
    secret_suffix = "homelab"
  }
}

provider "helm" {}

provider "kubectl" {}

provider "kubernetes" {}

provider "random" {}

locals {
  timezone = "Europe/Rome"
}
