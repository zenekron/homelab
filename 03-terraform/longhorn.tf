resource "kubernetes_namespace_v1" "longhorn" {
  metadata {
    name = "longhorn-system"
  }
}

module "longhorn" {
  source = "./modules/longhorn"

  namespace = kubernetes_namespace_v1.longhorn.metadata[0].name

  values = {
    persistence = {
      defaultClassReplicaCount = 1
      defaultDataLocality      = "best-effort"
    }

    defaultSettings = {
      storageMinimalAvailablePercentage = 25
      storageOverProvisioningPercentage = 200

      allowVolumeCreationWithDegradedAvailability = false
      replicaSoftAntiAffinity                     = false
    }
  }
}

resource "kubernetes_ingress_v1" "longhorn" {
  metadata {
    name      = "longhorn"
    namespace = kubernetes_namespace_v1.longhorn.metadata[0].name

    annotations = {
      "traefik.ingress.kubernetes.io/router.middlewares" = join(",", [
        module.traefik.middlewares.strip_prefix_1.id,
      ])
    }
  }

  spec {
    rule {
      http {
        path {
          path = "/longhorn"

          backend {
            service {
              name = module.longhorn.frontend_service.metadata[0].name

              port {
                name = module.longhorn.frontend_service.spec[0].port[0].name
              }
            }
          }
        }
      }
    }
  }
}
